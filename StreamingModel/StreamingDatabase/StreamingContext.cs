﻿using StreamingModel.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StreamingModel.StreamingDatabase
{
    public class StreamingContext : DbContext
    {
        public StreamingContext() :base("name=StreamingDb")
        {
                
        }
        public DbSet<StreamingService> StreamingServices { get; set; }

        public List<StreamingService> GetAllStreamingServices()
        {
            return StreamingServices.ToList();
        }

        public void AddStreamingService(StreamingService streamingService)
        {
            StreamingServices.Add(streamingService);
            SaveChanges();
        }
    }
}
