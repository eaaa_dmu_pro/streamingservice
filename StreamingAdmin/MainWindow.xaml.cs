﻿using StreamingModel.Model;
using StreamingModel.StreamingDatabase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StreamingAdmin
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private StreamingService _streamingService;
        public MainWindow()
        {
            InitializeComponent();
            _streamingService = new StreamingService();
            DataContext = _streamingService;
        }

        private void OnAddServiceCliked(object sender, RoutedEventArgs e)
        {
            using(var db = new StreamingContext())
            {
                db.AddStreamingService(_streamingService);
                db.SaveChanges();
                MessageBox.Show($"Der er oprettet en ny service med navn: {_streamingService.Name}");
                _streamingService = new StreamingService();
                DataContext = _streamingService;

            }
        }
    }
}
