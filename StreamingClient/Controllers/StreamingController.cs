﻿using StreamingModel.StreamingDatabase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StreamingClient.Controllers
{
    public class StreamingController : Controller
    {
        private StreamingContext _db = new StreamingContext();
        // GET: Streaming
        public ActionResult Index()
        {
            return View(_db.GetAllStreamingServices());
        }
    }
}